class Logger
  def initialize(file_name)
    @file_name = file_name<<Time.now.strftime("%Y-%m-%d--%H-%M-%S")
    @step = 1
  end

  #Write info to log file
  def log_info (message)
    write_to_log("INFO: ", message)
  end

  #Write info to log file
  def log_step (message)
    write_to_log("STEP #{@step}: ", message)
    @step += 1
  end

  #Write error to log file
  def log_error (message)
    write_to_log("ERROR: ", message)
  end

  #Private method, write to file
  private def write_to_log(log_option, message)
    File.open("Logs/"+@file_name + ".txt", "a+") do |f|
      f.write(log_option + message + "\n")
    end
  end

end

