require_relative '../../Base/base_class.rb'
require_relative 'row_model.rb'

class SearchResultPage
  def initialize driver
    @driver = driver
    @rows = @driver.find_elements(:xpath, ".//section[@data-log-location='vs_contractor_search']")
  end

  #List of freelancer objects
  def freelancers_list()
    array = Array.new
    @rows.each do |row|
      name = row.find_element(:css, ".freelancer-tile-name").attribute("innerText").strip
      title = row.find_element(:css, "div[class^='ellipsis']>strong[data-qa='tile_title']").attribute("innerText").strip
      overview = row.find_element(:css, ".row.m-sm-top.m-0-bottom>div[class=col-xs-12]").attribute("innerText").chomp("...").strip
      skills = Array.new
      skills = row.find_elements(:xpath, ".//div[@class='skills-section']//a/span[not(contains(.,'more'))][1]")
      array <<  Freelancer.new(name, title, skills, overview, row)
    end
    return array
  end
end
