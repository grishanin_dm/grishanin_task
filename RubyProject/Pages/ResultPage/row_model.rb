
#Freelancer object
class Freelancer
  attr_accessor :name, :title, :skills, :container

  def initialize(name, title, skills, overview, container)
    @name = name
    @title = title
    @overview = overview
    @skills = Array.new
    skills.each do |skill|
      @skills << skill.text
    end
    @container = container
  end

  def title
    @title
  end

  def name
    @name
  end

  def overview
    @overview
  end

end