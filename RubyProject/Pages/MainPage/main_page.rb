require_relative '../../Base/base_class.rb'

class MainPage

  def initialize driver
    @driver = driver
    @magnifying_glass_button = @driver.find_element(:css, '.navbar-collapse.d-none button[type="submit"]')
    @input_field = @driver.find_element(:css, '.navbar-collapse.d-none.d-lg-flex>.navbar-form input[name=q]')
  end

  #Search input
  def enter_search_query(value)
    @input_field.send_keys(value);
    @magnifying_glass_button.click
    self
  end

end
