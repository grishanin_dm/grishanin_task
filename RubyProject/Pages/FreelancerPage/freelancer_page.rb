
 class FreelancerPage
   attr_accessor :title, :skills, :overview

   def initialize driver
     @driver = driver
   end

   def title
     @driver.find_element(:xpath, "//span[contains(@class,'up-active-context-title')] | //h3[@class='m-sm-bottom ng-binding ng-scope']").text
   end

   def skills
     skills_webelement = @driver.find_elements(:xpath, "//div[@id='oProfilePage']//a[contains(@class,'o-tag-skill')]")
     skills = Array.new
     skills_webelement.each do |skill|
     skills << skill.text
     end
     return skills
   end

   def overview
     @driver.find_element(:xpath, "//p[@itemprop='description']").text
   end


 end