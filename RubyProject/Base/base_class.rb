require 'selenium-webdriver'
require 'logger'


class BaseClass

 def initialize (browser)
   @url = "http://www.upwork.com"
   @browser = browser
 end

 # Open browser, clear cookie and set page load
  def initialize_browser
    case @browser
    when :ff, :firefox
      @driver =  Selenium::WebDriver.for(:ff)
    when :chrome
      @driver = Selenium::WebDriver.for(:chrome)
    when :ie
      @driver = Selenium::WebDriver.for(:ie)
    when :safari
      @driver = Selenium::WebDriver.for(:safari)
    else
      puts "ERROR: Wrong browser name!!"
    end
    clear_browser_cookies
    page_load
    implicit_wait
  end

 def clear_browser_cookies
   @driver.manage.delete_all_cookies
 end

 def navigate_to
   @driver.navigate.to @url
 end

 def implicit_wait
 @driver.manage.timeouts.implicit_wait = 10
 end

 def page_load
 @driver.manage.timeouts.page_load = 15
 end

  def base_teardown
    @driver.quit
  end

  def get_driver
    @driver
  end

  def url
    @url
  end

 def current_url
 @driver.current_url
 end

  end