
require_relative '../Tests/search_page.rb'

#Testsuite for verifying search page and freelancer page
class TestSuiteSearchPage
  @keyword = "QA"
  @browser = :ff

  @search_page = VerifySearchResult.new(@browser,@keyword)
  @search_page.tc01_verify_search_results
  @search_page.tc02_verify_freelancer_page

end