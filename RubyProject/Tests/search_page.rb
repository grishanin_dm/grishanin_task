require_relative '../Pages/MainPage/main_page.rb'
require_relative '../Base/base_class.rb'
require_relative '../Utils/logger.rb'
require_relative '../Pages/ResultPage/search_result_page.rb'
require_relative '../Pages/FreelancerPage/freelancer_page.rb'

require 'selenium-webdriver'

class VerifySearchResult
  #Prepare data for performing automation scenario
  # Declaration variables and instances
  def initialize (browser, keyword)
    @logger = Logger.new("VerifySearchResult")
    @keyword = keyword
    @freelancers
    @random_freelancer
    @freelancer_page
    @base_class = BaseClass.new(browser)
    @base_class.initialize_browser
    @logger.log_step("Open browser #{@browser}")
    @base_class.navigate_to
    @logger.log_step("Navigate to #{@base_class.url}")
  end


  def tc01_verify_search_results
    enter_search_query(@keyword)
    get_list_of_freelancers
    verify_freelancer_has_keyword
  end

  def tc02_verify_freelancer_page
    open_freelaner_page
    verify_skills
    verify_title
   # @base_class.base_teardown
  end

  #Find search input and type keyword
  def enter_search_query(keyword)
    MainPage.new(@base_class.get_driver).enter_search_query(keyword)
    @logger.log_step("Enter keyword to search field #{@keyword} and press magnifying glass button")
  end

  #Get list of freelancers on result page
  def get_list_of_freelancers
    @freelancers = SearchResultPage.new(@base_class.get_driver).freelancers_list
  end

  #Verify title has keyword
  #Write to log all users from first page with titles
  def verify_title_has_keyword (freelancer)
    if freelancer.title.include? @keyword
      @logger.log_info('Freelancer ' + freelancer.name + ' title: ' + freelancer.title)
      return true
    else
      @logger.log_info('Freelancer ' + freelancer.name + ' does not have ' + @keyword + ' in title')
      return false
    end
  end

  #Verify skills has keyword
  #Write to log all users with skill which has keyword
  #Write to log in case user does not have skills at all
  def verify_skills_has_keyword (freelancer)
    if freelancer.skills.nil?
      @logger.log_info('Freelancer ' + freelancer.name + ' does not have skills')
      return false
    elsif freelancer.skills.any? {|skill|
      if skill.include? @keyword
        @logger.log_info('Freelancer ' + freelancer.name + ' has skill: ' + skill)
        return true
      else
        @logger.log_info('Freelancer ' + freelancer.name + ' does not have ' + @keyword + ' in skills')
        return false
      end
    }
    end
  end

  #Verify overview has keyword
  #Write to log all users from first page with overview
  def verify_overview_has_keyword (freelancer)
    if freelancer.overview.include? @keyword
      @logger.log_info('Freelancer ' + freelancer.name + ' overview: ' + freelancer.overview)
      return true
    else
      @logger.log_info('Freelancer ' + freelancer.name + ' does not have ' + @keyword + ' in overview')
      return false
    end
  end


  #Get random freelancer from search page and click
  #Freelancer page should be opened
  def open_freelaner_page
    @random_freelancer = @freelancers.sample
    @logger.log_step("Open freelancer page #{@random_freelancer.name} " + @base_class.current_url)
    @random_freelancer.container.click
    @freelancer_page = FreelancerPage.new(@base_class.get_driver)
  end

  #Verify skills on freelancer page
  #All skills from search page should be displayed on freelancer page
  def verify_skills
    @logger.log_step("Verify freelancer page. Page should has the same skills like on search page")
    if @random_freelancer.skills.all? {|x| @freelancer_page.skills.include?(x)}
      @logger.log_info("Freelancer page has the same skills like on search page")
    else
      @logger.log_error("Skills are not equal")
      @logger.log_error("--Skills on search page: " + "\n" + +@random_freelancer.skills.join("\n") + "\n" +
                            "--Skills on freelancer page: " + "\n" + +@freelancer_page.skills.join("\n"))
    end
  end

  #Verify title on freelancer page
  #Title from search page should be the same like on freelancer page
  def verify_title
    @logger.log_step("Verify freelancer page. Page should has the same title like on search page")
    if @random_freelancer.title.eql? @freelancer_page.title
      @logger.log_info("Freelancer page has the same title like on search page")
    else
      @logger.log_error("Title is differ" + "\n" +
                            "--Actual result: " + @random_freelancer.title + "\n" +
                            "--Expected result: " + @freelancer_page.title)
    end
  end

  #Verify overview on freelancer page
  #Overview from search page should  starts the same like on freelancer page
  def verify_overview
    @logger.log_step("Verify freelancer page. Page should has the same overview like on search page")
    if @random_freelancer.overview.eql? @freelancer_page.overview
      @logger.log_info("Freelancer page has the same overview like on search page")
    else
      @logger.log_error("Overview is differ" + "\n" +
                            "--Actual result: " + @random_freelancer.overview + "\n" +
                            "--Expected result: " + @freelancer_page.overview)
    end
  end

  def verify_freelancer_has_keyword
    @logger.log_step("Verify attributes on search page (Title, Skills, Overview)")
    @freelancers.each do |freelancer|
      title_has_keyword = verify_title_has_keyword(freelancer)
      skills_has_keyword = verify_skills_has_keyword(freelancer)
      overview_has_keyword = verify_overview_has_keyword(freelancer)
      if !title_has_keyword && !skills_has_keyword && !overview_has_keyword
        @logger.log_error("Freelancer " + freelancer.name + " does not have keyword: #{@keyword} in attributes")
      end
    end
  end

end
